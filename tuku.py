# -*- coding:utf-8 -*-
    import urllib2
    # 個人設定画面に表示されているAPIキー
    api_key = '62124b372d6f972aabe3745c2f62894b68e4c1ab'
    # 登録用のデータ
    xml = """<?xml version="1.0"?>
    <issue>
     <project_id>test_project</project_id>
     <subject>自動登録テスト</subject>
     <description>チケットの詳細です</description>
     <tracker_id>2</tracker_id>
    </issue>"""
    # APIのURL
    # 今回、RedmineのURLはhttp://192.168.1.102/redmine/
    # サブディレクトリで公開している
    url = 'http://10.202.115.160:8080/redmine/issues.xml'
    # Content-Type:text/xml
    # X-Redmine-API-Key:[APIキー]
    # method:post
    request = urllib2.Request(url, data=xml)
    request.add_header('Content-Type', 'text/xml')
    request.add_header('X-Redmine-API-Key', api_key)
    request.get_method = lambda: 'POST'
    # 登録実行
    response = urllib2.urlopen(request)
    ret = response.read()
    print 'Response:', ret
