# -*- coding: utf-8 -*-
import datetime
from redmine import Redmine
redmine = Redmine('http://10.202.115.160:8080/redmine/',
key = '62124b372d6f972aabe3745c2f62894b68e4c1ab')
issue = redmine.issue.new()
issue.project_id = 'simokawabe'
issue.subject = 'サブジェクト'
issue.tracker_id = 1     #トラッカー
issue.description = 'チケットの内容をしめす。\n改行もできる。'
issue.status_id = 1      #ステータス
issue.priority_id = 1    #優先度
issue.assigned_to_id = 1 #担当者のID
issue.watcher_user_ids = [1,3] # ウォッチするユーザのID
issue.parent_issue_id = 12     # 親チケットのID
issue.start_date = datetime.date(2016, 12, 19) #開始日
issue.due_date = datetime.date(2014, 12, 21)   #期日
issue.estimated_hours = 4   # 予想工数
issue.done_ratio = 40
issue.custom_fields = [{'id': 1, 'value': 'foo'}]
issue.uploads = [{'path': '/share/test.txt'}]
issue.save()
