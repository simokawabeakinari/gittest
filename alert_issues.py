# coding: utf-8
from redmine import Redmine
import json
import datetime

redmine = Redmine('http://10.202.115.160:8080/redmine/', key='62124b372d6f972aabe3745c2f62894b68e4c1ab')
redmine_path = "/home/akinari/ai/settings/"

f = open(redmine_path + "watcher_template.json", 'r', encoding="utf-8")
watcher_info = json.load(f)
f.close()

today_dict = {}
today_number = 0
past_dict = {}
past_number = 0
issue_dict = {}

issues = redmine.issue.all(sort='priority_id:desc')
for issue in issues:
    if issue.status["id"] in [5, 6, 12]:
        continue

    today_flag = False
    past_flag = False
    try:
        if issue.due_date is not None:
            if issue.due_date == datetime.date.today():
                today_flag = True
                today_number += 1
            elif issue.due_date < datetime.date.today():
                past_flag = True
                past_number += 1

            if today_flag is True or past_flag is True:
                issued_project = redmine.project.get(issue.project["id"])
                got_issue_dict = {
                    "issue_id": issue.id,
                    "subject": issue.subject,
                    "project_name": issued_project.name
                }

                if today_flag is True and issued_project.identifier in today_dict:
                    today_dict[issued_project.identifier].append(got_issue_dict)
                elif past_flag is True and issued_project.identifier in past_dict:
                    past_dict[issued_project.identifier].append(got_issue_dict)
                elif today_flag is True and issued_project.identifier not in today_dict:
                    today_dict[issued_project.identifier] = [got_issue_dict]
                elif past_flag is True and issued_project.identifier not in past_dict:
                    past_dict[issued_project.identifier] = [got_issue_dict]
    except:
        pass


def get_issue_description(issue_dict):
    '''
    Args:
        issue_dict:   次の形式の辞書を格納したリスト。
                    [
                      {
                        "project_name": "{プロジェクト名}", 
                        "subject": "{チケット名}",
                        "issue_id": "{チケットの固有ID}"
                      }
                    ]
    Returns:
        チケットの内容
    '''
    issue_description = ""
    issue_description += "\n---------------------------------------------------------------\n"
    issue_description += "■ プロジェクト：" + issue_dict[0]["project_name"] + "\n"
    issue_description += project_url_template + project_identifier + "\n"
    issue_description += "\n○ チケット一覧\n\n"
    for issue_data in issue_dict:
        issue_description += issue_data["subject"] + "\n"
        issue_description += issue_url_template + str(issue_data["issue_id"]) + "\n\n"
    issue_description += "\n\n"
    return issue_description

target_project_id = "{simokawabe}"
today_datetime_string = datetime.date.today().strftime("%Y-%m-%d")
project_url_template = "http://redmine.domain.foo/projects/"
issue_url_template = "http://redmine.domain.foo/issues/"

issue_description = "各位\n\nお疲れ様です。PM代行botです。\n\n"
issue_description += "本日が締切日となっているチケットの一覧を共有致します。\n\n"
issue_description += "該当するチケットは、" + str(today_number) + "件ございます。\n\n"

for project_identifier in today_dict.keys():
    issue_description += get_issue_description(today_dict[project_identifier])

today_issue = redmine.issue.new()
today_issue.project_id = target_project_id
today_issue.subject = "本日が締切日となっているチケットの一覧（対象：全社、観測日：" + today_datetime_string + "）"
today_issue.description = issue_description
today_issue.save()

issue_description = "各位\n\nお疲れ様です。PM代行botです。\n\n"
issue_description += "本日の現時点で既に締切日が過ぎているチケットの一覧を共有致します。\n\n"
issue_description += "該当するチケットは、" + str(past_number) + "件ございます。\n\n"

for project_identifier in past_dict.keys():
    issue_description += get_issue_description(past_dict[project_identifier])

past_issue = redmine.issue.new()
past_issue.project_id = target_project_id
past_issue.subject = "締切日が過ぎているチケットの一覧（対象：全社、観測日：" + today_datetime_string + "）"
past_issue.description = issue_description
past_issue.save()
